#!/bin/bash

. lib.sh

# FORMAT="jpg"
FORMAT="png"
# FORMAT="svg"

ignore=(json pipe cutils journal)

__make_dependency_graph_main(){

    PROJECT_LIST=( $( cat projects.csv | tr ';' '\n'  | sort ) )

    __clone_projects

    __remove_flagged_projects

    FILE="${TMP_DIR}/test.dot"

    echo "digraph G {" > $FILE
    # echo "    layout=dot" >> $FILE
    echo "    rankdir=LR" >> $FILE

    local i
    for i in ${!PROJECT_LIST[@]} ; do

        if ! $MAIN_RUNNING ; then
            __exit
        fi

        local PROJ_NAME=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 1)
        local PROJ_DIR=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 3)

        cd "${REPO_DIR}/${PROJ_NAME}"

        echo >> $FILE
        echo -n "    \"${PROJ_NAME}\" " >> $FILE
        case ${PROJ_DIR} in
            "services")
                # echo "Services"
                echo -n "[shape=ellipse color=blue ]" >> $FILE
                ;;

            "core-libs")
                # echo "Core Libs"
                echo -n "[shape=box color=green ]" >> $FILE
                ;;

            "utilities")
                # echo "Utilities"
                echo -n "[shape=hexagon color=turquoise ]" >> $FILE
                ;;

            "third-party")
                # echo "Third Party"
                echo -n "[shape=box color=orange ]" >> $FILE
                ;;
        esac
        echo ";" >> $FILE

        if ! [ -f pkg/control/control ] ; then
            echo "No Control File"
            continue
        fi

        if cat pkg/control/control | grep -q Depends ; then

            local deps=($(cat pkg/control/control | grep Depends | sed "s/Depends://g" | tr ',' '\n' | tr '|' '\n' | tr -d ' ' | cut -d '(' -f 1 | sed '/^$/d'))
            for i in ${!deps[@]} ; do
                echo "    \"${deps[i]}\" -> \"$PROJ_NAME\";" >> $FILE
            done
        fi


        if cat pkg/control/control | grep -q Provides ; then

            # Opencv provides a fat stack os stuff we don't want to see here
            if echo ${PROJ_NAME} | grep -qi opencv ; then
                continue
            fi

            # Don't care about services providing things
            if echo ${PROJ_DIR} | grep -qi services ; then
                continue
            fi

            local provs=($(cat pkg/control/control | grep Provides | sed "s/Provides://g" | tr ',' '\n' | tr -d ' ' | cut -d '(' -f 1 | sed '/^$/d'))
            for i in ${!provs[@]} ; do
                if ! cat $FILE | grep -q "\"${provs[i]}" ; then
                    echo -n "    \"${provs[i]}\" " >> $FILE
                    echo -n "[shape=doubleoctagon color=purple ]" >> $FILE
                    echo ";" >> $FILE
                fi

                echo "    \"$PROJ_NAME\" -> \"${provs[i]}\";" >> $FILE
            done
        fi

    done
    echo "}" >> $FILE

    cd $HOME_DIR

    cp $FILE body.dot

    # ----- Pruning ------
    # Get rid of the things we're ignoring
    mv body.dot test.dot
    cat test.dot | egrep -v "$(echo -n ${ignore[@]} | tr ' ' '|' )" > body.dot

    # Get rid of virtual packages with nothing using them
    virts=($(cat test.dot | grep purple | tr -s ' ' | cut -d ' ' -f 2 | tr -d '"'))
    for i in ${!virts[@]} ; do
        if ! cat body.dot | grep -q "\"${virts[i]}\" ->" ; then
            mv body.dot test.dot
            cat test.dot | grep -v "\"${virts[i]}\"" > body.dot
        fi
    done

    # Generate the actual output image
    rm -f test.dot
    cat body.dot | dot -T$FORMAT > body.$FORMAT

    echo "    graph legend {" > legend.dot
    # echo "        rankdir=LR" >> legend.dot
    echo "        \"Service\" [shape=ellipse color=blue ]" >> legend.dot
    echo "        \"Core Lib\" [shape=box color=green ]" >> legend.dot
    echo "        \"Third Party Lib\" [shape=box color=orange ]" >> legend.dot
    echo "        \"Tool/Utility\" [shape=hexagon color=turquoise ]" >> legend.dot
    echo "        \"Virtual Package\" [shape=doubleoctagon color=purple ]" >> legend.dot
    echo "    }" >> legend.dot

    cat legend.dot | dot -T$FORMAT > legend.$FORMAT

    convert legend.png body.png -append output.png

}

__make_dependency_graph_main $@
