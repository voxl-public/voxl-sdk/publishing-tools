#!/bin/bash

. lib.sh

mkdir -p $REPO_DIR
mkdir -p $TMP_DIR

SHOW_PLATFORMS=false
MODE="TTY"

NEW=""
OLD=""

__print_usage () {
    echo "Command line arguments are as follows:"
    echo "-h, --help              : Print this help message"
    echo "-m, --markdown          : Output is in a markdown-friendly table instead of human-readable one"
    echo "-p, --platforms         : Add columns for the supported platforms"
    echo
    echo "Sample usage: ./generate-change-table.sh projects-0.7.csv projects-0.5.csv -p"
    echo
}


__parse_opts(){

    while [[ $# -gt 0 ]]; do
        case $1 in
            -f|--full)
                MODE="FULL"
                ;;
            -h|--help)
                # Make sure we don't purge the relics on a false run
                __print_usage
                exit 0
                ;;
            -m|--markdown)
                MODE="MARKDOWN"
                ;;
            -p|--platforms)
                SHOW_PLATFORMS=true
                ;;
            *)
                if [[ "$NEW" == "" ]] ; then
                    NEW="$1"
                elif [[ "$OLD" == "" ]] ; then
                    OLD="$1"
                else
                    # Make sure we don't purge the relics on a false run
                    echo "Unknown argument $1"
                    __print_usage
                    exit 1
                fi
                ;;
        esac
        shift
    done
}

__run_tty(){


    PROJECT_LIST=( $( cat ${NEW} | tr ';' '\n'  | sort ) )

    echo -e "  ${CLR_LIT_GRN}No Changes${RESET_ALL}  ${CLR_LIT_YLW}New Version${RESET_ALL}  ${CLR_LIT_RED}New Package${RESET_ALL}"
    echo

    if echo -n ; then
        for PROJECT_LINE in ${PROJECT_LIST[@]} ; do

            PROJ_NAME=$(echo ${PROJECT_LINE} | cut -d ',' -f 1)
            PROJ_VER=$(echo ${PROJECT_LINE}  | cut -d ',' -f 2)
            PROJ_DIR=$(echo ${PROJECT_LINE}  | cut -d ',' -f 3)
            PROJ_APQ=$(echo ${PROJECT_LINE}  | cut -d ',' -f 4)
            PROJ_QRB=$(echo ${PROJECT_LINE}  | cut -d ',' -f 5)

            if ! [ $PROJ_APQ == "TRUE" ] && ! [ $PROJ_QRB == "TRUE" ] ; then
                continue
            fi


            if cat ${OLD} | grep -q "${PROJ_NAME}," ; then
                OLD_VER="$( cat ${OLD} | tr ';' '\n' | grep "${PROJ_NAME}," | cut -d ',' -f 2)"

                if [[ " $OLD_VER " == " $PROJ_VER " ]] ; then

                    echo -en "| ${CLR_LIT_GRN}${PROJ_NAME}${RESET_ALL} | ${PROJ_VER}"
                else

                    echo -en "| ${CLR_LIT_YLW}${PROJ_NAME}${RESET_ALL} | ${OLD_VER} --> ${PROJ_VER}"
                fi

            else
                echo -en "| ${CLR_LIT_RED}${PROJ_NAME}${RESET_ALL} | ${PROJ_VER}"
            fi

            echo -n " | "
            if [ $PROJ_APQ == "TRUE" ] ; then
                echo -en "\U2705" # UTF checkbox
            fi
            echo -n " | "
            if [ $PROJ_QRB == "TRUE" ] ; then
                echo -en "\U2705" # UTF checkbox
            fi
            echo

        done
    fi | column -t -s '|'  $(if ! $SHOW_PLATFORMS ; then echo -n "-H APQ8096,QRB5165" ; fi)

}

__run_markdown(){


    PROJECT_LIST=( $( cat ${NEW} | tr ';' '\n'  | sort ) )

    echo -n "| "
    echo -n "Package"
    echo -n " | "
    echo -n "Version"

    if $SHOW_PLATFORMS ; then
        echo -n " | "
        echo -n "APQ8096"
        echo -n " | "
        echo -n "QRB5165"
    fi

    echo  "|"
    echo -n "|"
    echo -n ":---"
    echo -n "|"
    echo -n ":---:"

    if $SHOW_PLATFORMS ; then
        echo -n "|"
        echo -n ":---:"
        echo -n "|"
        echo -n ":---:"
    fi
    echo  "|"

    for PROJECT_LINE in ${PROJECT_LIST[@]} ; do

        PROJ_NAME=$(echo ${PROJECT_LINE} | cut -d ',' -f 1)
        PROJ_VER=$(echo ${PROJECT_LINE}  | cut -d ',' -f 2)
        PROJ_DIR=$(echo ${PROJECT_LINE}  | cut -d ',' -f 3)
        PROJ_APQ=$(echo ${PROJECT_LINE}  | cut -d ',' -f 4)
        PROJ_QRB=$(echo ${PROJECT_LINE}  | cut -d ',' -f 5)

        if ! [ $PROJ_APQ == "TRUE" ] && ! [ $PROJ_QRB == "TRUE" ] ; then
            continue
        fi

        echo -n "| "
        echo -n "[${PROJ_NAME}](https://gitlab.com/voxl-public/voxl-sdk/${PROJ_DIR}/${PROJ_NAME})"
        echo -n " | "

        if cat ${OLD} | grep -q "${PROJ_NAME}," ; then
            OLD_VER="$( cat ${OLD} | tr ';' '\n' | grep "${PROJ_NAME}," | cut -d ',' -f 2)"

            if [[ " $OLD_VER " == " $PROJ_VER " ]] ; then
                echo -n "${PROJ_VER} (unchanged)"
            else
                echo -n "${OLD_VER} --> ${PROJ_VER}"
            fi

        else
            echo -n "${PROJ_VER} (new)"
        fi

        if $SHOW_PLATFORMS ; then
            echo -n " | "
            if [ $PROJ_APQ == "TRUE" ] ; then
                echo -en "\U2705" # UTF checkbox
            fi
            echo -n " | "
            if [ $PROJ_QRB == "TRUE" ] ; then
                echo -en "\U2705" # UTF checkbox
            fi
        fi

        echo " |"

    done

}

__run_full(){


    PROJECT_LIST=( $( cat ${NEW} | tr ';' '\n'  | sort ) )

    clear
    __clone_projects

    for PROJECT_LINE in ${PROJECT_LIST[@]} ; do

        PROJ_NAME=$(echo ${PROJECT_LINE} | cut -d ',' -f 1)
        PROJ_VER=$(echo ${PROJECT_LINE}  | cut -d ',' -f 2)
        PROJ_DIR=$(echo ${PROJECT_LINE}  | cut -d ',' -f 3)
        PROJ_APQ=$(echo ${PROJECT_LINE}  | cut -d ',' -f 4)
        PROJ_QRB=$(echo ${PROJECT_LINE}  | cut -d ',' -f 5)

        if [ $PROJ_APQ == "FALSE" ] && [ $PROJ_QRB == "FALSE" ] ; then
            continue
        fi

        if ! [ -f ${REPO_DIR}/${PROJ_NAME}/CHANGELOG ] ; then
            echo -e "${SET_BOLD}${CLR_RED}------ $PROJ_NAME ------${RESET_ALL}"
            echo -e "    Missing changelog"
            echo
            continue
        fi

        if ! cat ${REPO_DIR}/${PROJ_NAME}/CHANGELOG | grep -q ${PROJ_VER} ; then
            echo -e "${SET_BOLD}${CLR_RED}------ $PROJ_NAME ------${RESET_ALL}"
            echo -e "    Missing changelog entry with expected version"
            echo
            continue
        fi

        if cat ${OLD} | grep -q "${PROJ_NAME}," ; then
            OLD_VER="$( cat ${OLD} | tr ';' '\n' | grep "${PROJ_NAME}," | cut -d ',' -f 2)"

            if ! [[ " $OLD_VER " == " $PROJ_VER " ]] ; then

                if ! cat ${REPO_DIR}/${PROJ_NAME}/CHANGELOG 2>/dev/null | grep -q ${OLD_VER} ; then
                    local maj=$( echo $OLD_VER | cut -d '-' -f 1 | cut -d '.' -f 1 )
                    local min=$( echo $OLD_VER | cut -d '-' -f 1 | cut -d '.' -f 2 )
                    local pat=$( echo $OLD_VER | cut -d '-' -f 1 | cut -d '.' -f 3 )
                    local fix

                    if echo $OLD_VER | grep -q '-' ; then
                        fix=$( echo $OLD_VER | cut -d '-' -f 2 )
                    fi

                    while ! cat ${REPO_DIR}/${PROJ_NAME}/CHANGELOG 2>/dev/null | grep -q ${OLD_VER} ; do
                        if [[ $fix ]] ; then
                            if [[ ( $fix == 0 ) ]] ; then
                                fix=50
                                if [[ ( $pat == 0 ) ]] ; then
                                    pat=50
                                    if [[ ( $min == 0 ) ]] ; then
                                        min=50
                                        if [[ ( $maj == 0 ) ]] ; then
                                            continue 2
                                        else
                                            maj=$((maj-1))
                                        fi
                                    else
                                        min=$((min-1))
                                    fi
                                else
                                    pat=$((pat-1))
                                fi
                            else
                                fix=$((fix-1))
                            fi
                            OLD_VER="${maj}.${min}.${pat}-${fix}"
                        else
                            if [[ ( $pat == 0 ) ]] ; then
                                pat=50
                                if [[ ( $min == 0 ) ]] ; then
                                    min=50
                                    if [[ ( $maj == 0 ) ]] ; then
                                        continue 2
                                    else
                                        maj=$((maj-1))
                                    fi
                                else
                                    min=$((min-1))
                                fi
                            else
                                pat=$((pat-1))
                            fi
                            OLD_VER="${maj}.${min}.${pat}"
                        fi
                    done
                    echo -e "${SET_BOLD}${CLR_YLW}------ $PROJ_NAME ------${RESET_ALL}"
                else
                    echo -e "${SET_BOLD}${CLR_GRN}------ $PROJ_NAME ------${RESET_ALL}"
                fi

                cat ${REPO_DIR}/${PROJ_NAME}/CHANGELOG | awk "/${PROJ_VER}/{f=1} /${OLD_VER}/{f=0} f"
                echo
            fi

        else
            echo -e "${SET_BOLD}${CLR_GRN}------ $PROJ_NAME ------${RESET_ALL}"
            cat ${REPO_DIR}/${PROJ_NAME}/CHANGELOG
            echo
        fi

    done
}

__generate_change_table_main() {

    __parse_opts $@

    if [[ "$NEW" == "" ]] || [[ "$OLD" == "" ]] ; then
        echo "ERROR: Wasn't given file(s) to parse"
        exit 1
    fi

    if ! [ -f "$NEW" ]; then
        echo "ERROR: Couldn't find new file: ${NEW}, make sure this file is available"
        exit 1
    fi

    if ! [ -f "$OLD" ]; then
        echo "ERROR: Couldn't find old file: ${OLD}, make sure this file is available"
        exit 1
    fi

    case $MODE in
        TTY )
            __run_tty
            ;;

        MARKDOWN )
            __run_markdown
            ;;

        FULL )
            __run_full
            ;;

        *)
            echo "Invalid mode: $MODE"
            exit -1
            ;;
    esac
}

__generate_change_table_main "$@"
