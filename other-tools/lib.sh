
. .bash_formatting

REPO_DIR="/tmp/sdk-publisher/repos"
TMP_DIR="/tmp/sdk-publisher/tmp"
HOME_DIR="$(pwd)"

handler()
{
    MAIN_RUNNING=false
}

__clean() {
    [ -d $REPO_DIR ] && rm -rf $REPO_DIR
    [ -d $TMP_DIR ] && rm -rf $TMP_DIR
}


__exit() {

    local EXIT_COLOR
    if [ $# -eq 0 ] ; then
        EXIT_COLOR="${CLR_LIT_YLW}"
    elif [ $1 -eq 0 ]; then
        EXIT_COLOR="${CLR_LIT_GRN}"
    else
        EXIT_COLOR="${CLR_LIT_RED}"
    fi

    echo
    echo -e "${EXIT_COLOR}${SET_BOLD}----Exiting----${RESET_ALL}"
    echo

    exit $1
}

__pull_projects(){
    echo
    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Pulling Updates----${RESET_ALL}"


    local LOCATION_X=$(cursor-col)
    local LOCATION_Y=$(cursor-row)

    local i
    for i in ${!PROJECT_LIST[@]} ; do

        if ! $MAIN_RUNNING ; then
            __exit
        fi

        local PROJ_NAME=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 1)
        local PROJ_DIR=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 3)

        __gotoxy $LOCATION_X $LOCATION_Y
        echo -e "${CLEAR_LINE}${PROJ_NAME}"

        __print_progress_bar ${i} "${#PROJECT_LIST[@]}"

        cd "${REPO_DIR}/${PROJ_NAME}"

        git pull -p > /dev/null 2> /dev/null

    done

    __gotoxy $LOCATION_X $LOCATION_Y
    __print_progress_bar 1 1

    echo
    echo -e "${CLEAR_LINE}${CLR_LIT_GRN}${SET_BOLD}----Success----${RESET_ALL}"
}

__clone_projects() {

    clear

    SAFE_LIST=()
    ERROR_CLONE_LIST=()

    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Cloning Projects----${RESET_ALL}"
    for i in ${!PROJECT_LIST[@]} ; do

        PROJ_NAME=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 1)
        SAFE_LIST+=("${PROJ_NAME}")
        PROJ_VER=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 2)
        PROJ_DIR=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 3)

        __gotoxy 0 2
        echo -e "${CLEAR_LINE}${PROJ_NAME}"

        __print_progress_bar ${i} "${#PROJECT_LIST[@]}"

        SSH_ADDR="git@gitlab.com:voxl-public/voxl-sdk/${PROJ_DIR}/${PROJ_NAME}.git"


        cd $REPO_DIR
        if ! [ -d $PROJ_NAME ] ; then
            git clone -q $SSH_ADDR 2>"${TMP_DIR}/git_dump"
            if ! [[ $? -eq 0 ]] ; then
                ERROR_CLONE_LIST+=("${PROJ_NAME}")
                SAFE_LIST=( "${SAFE_LIST[@]/$PROJ_NAME}" )
                continue
            fi
            cd $PROJ_NAME
            # git checkout dev >/dev/null 2>/dev/null
        fi
        cd $HOME_DIR

        ! $MAIN_RUNNING && echo && return 1

    done

    __gotoxy 0 2
    __print_progress_bar 1 1
    echo
    echo -en "${CLEAR_LINE}"

    if [[ ${#ERROR_CLONE_LIST[@]} -gt 0 ]] ; then

        echo -e "${CLR_LIT_RED}${SET_BOLD}----Encountered Errors Cloning Projects----${RESET_ALL}"
        echo ${ERROR_CLONE_LIST[@]} | tr ' ' '\n' | sort | column

        return 1
    fi


    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Succcess----${RESET_ALL}"
}


## remove packages that are not in either APQ or QRB platforms
## not currently used
__remove_flagged_projects() {

    for i in ${!PROJECT_LIST[@]} ; do
        PROJ_APQ=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 4)
        PROJ_QRB=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 5)

        if [ $PROJ_APQ == "FALSE" ] && [ $PROJ_QRB == "FALSE" ] ; then
            unset PROJECT_LIST[$i]
        fi
    done
    PROJECT_LIST=("${PROJECT_LIST[@]}")
}

MAIN_RUNNING=true
trap handler SIGINT
