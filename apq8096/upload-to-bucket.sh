#!/bin/bash

# Exit if any command has error
set -e


print_usage() {
	echo ""
	echo "Usage:"
	echo "      ./upload-to-bucket.sh nightly CI_KEY CI_URI"
	echo "      ./upload-to-bucket.sh weekly CI_KEY CI_URI"
	echo ""
}

if [[ "$#" -lt 2 ]]; then
	echo ""
	echo "[WARNING] not updating bunker records"
fi
TYPE=$1
CI_KEY=$2
CI_URI=$3

# check if it's a nightly or weekly upload
if [[ "$TYPE" == "nightly" ]]; then
	echo "detected nightly build"
	BUCKET_NAME="gs://platform-nightlies"
elif [[ "$TYPE" == "weekly" ]]; then
	echo "detected weekly build"
	BUCKET_NAME="gs://platform-weeklies"
else
	echo "invalid argument, please specify weekly or nightly as the first argument"
	print_usage
	exit 1
fi

# this script is to be ran after make-platform-release.sh
ARCHIVE_NAME=$(find . -type f -name '*.tar.gz' -print -quit)
BUILD_NAME=$(basename "$ARCHIVE_NAME")

if [ -z "$ARCHIVE_NAME" ]; then
	echo "No archive to upload"
	exit 1
else
	echo "Using archive: ${BUILD_NAME}"
fi



BOARD_NAME="voxl"

echo ">> uploading ${BUILD_NAME}"
echo "to ${BUCKET_NAME}/${BOARD_NAME}/"
gsutil cp "${BUILD_NAME}" "${BUCKET_NAME}/${BOARD_NAME}/"
echo ">> done uploading"
