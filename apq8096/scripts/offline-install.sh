#!/bin/bash

PKG_DIR=/data/voxl-suite-offline-packages

# Sanity check adb is installed
FILE=$(which adb)
if [ -f $FILE ]; then
    echo "[INFO] adb installed"
else
    echo ""
    echo "[Error]: android-tools-adb missing, use: apt-get install android-tools-adb"
    echo ""
    exit 1
fi


# Connect and create directory to push IPKs to on target
echo "[INFO] Waiting for device"
adb wait-for-device

# push files over
echo "[INFO] Pushing package manifest to target"
adb shell rm -rf $PKG_DIR
adb shell mkdir -p $PKG_DIR
adb push Packages.gz $PKG_DIR/.
for FILE in *.ipk; do
    adb push $FILE $PKG_DIR/.
done

# Set up opkg for local installation
FILE="/etc/opkg/opkg.conf"
LINE="src/gz local file://${PKG_DIR}"

## wipe opkg config and set it up clean
echo "Setting up $FILE"

adb shell "echo '################################################################' > $FILE"
adb shell "echo '## This file has been automatically generated.' >> ${FILE}"
adb shell "echo '## Please use voxl-configure-pkg-manager to modify it.' >> ${FILE}"
adb shell "echo '################################################################' >> ${FILE}"
adb shell "echo 'dest root /' >> ${FILE}"
adb shell "echo 'option lists_dir /var/lib/opkg/lists' >> ${FILE}"
adb shell "echo >> ${FILE}"
adb shell "echo ${LINE} >> ${FILE}"
adb shell "echo >> ${FILE}"

echo "[INFO] updating opkg package list"
adb shell opkg update

## just tell opkg to install the latest voxl-suite which is probably the one
## we just pushed. Don't tell it to install that specific package because then
## the package will be flagged in OPKG's memory as being stuck at that version
## and you won't be able to upgrade smoothly later

echo "[INFO] installing voxl-suite"
adb shell opkg install --force-reinstall voxl-suite

echo "[INFO] Pointing to correct repo"
adb shell "voxl-configure-pkg-manager DUMMY-SDK-REPO-TARGET"

echo "[INFO] Done installing voxl-suite"

