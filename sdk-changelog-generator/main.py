#!/usr/bin/python3

import atexit
import base64
import gzip
import os
import re
import subprocess
import tempfile
from tkinter import *
from tkinter import Entry, Label, filedialog

import gitlab
import requests
from bs4 import BeautifulSoup as Soup

BASE_URL = "http://voxl-packages.modalai.com/dists/"


def open_with_editor(path):
    if os.path.exists("/opt/sublime_text/sublime_text"):
        subprocess.Popen(["/opt/sublime_text/sublime_text", path])
    elif os.path.exists("/usr/bin/gedit"):
        subprocess.Popen(["gedit", path])


class MyApp:
    def __init__(self, root) -> None:
        root.title("Modal AI CHANGELOG generator")
        my_menu = Menu(root)
        root.config(menu=my_menu)

        # Create a submenu for "File" under my_menu
        file_menu = Menu(my_menu)
        my_menu.add_cascade(label="File", menu=file_menu)
        
        # Add the Help command to the file_menu submenu
        file_menu.add_command(label="Help", command=self.help)
        
        self.clicked_sdk1 = StringVar()
        self.clicked_sdk1.set("sdk-1.0/")
        self.clicked_sdk2 = StringVar()
        self.clicked_sdk2.set("staging/")
        self.clicked_file1 = StringVar()
        self.clicked_file1.set("Packages")
        self.clicked_file2 = StringVar()
        self.clicked_file2.set("Packages")
        self.clicked_release = StringVar()
        self.clicked_release.set("qrb5165/")

        # Bind the callback function to the clicked_release variable
        self.clicked_release.trace_add("write", self.update_sdk_and_files)
        self.clicked_sdk1.trace_add("write", self.update_sdk_and_files)
        self.clicked_sdk2.trace_add("write", self.update_sdk_and_files)
        self.clicked_file2.trace_add("write", self.update_sdk_and_files)
        self.clicked_file1.trace_add("write", self.update_sdk_and_files)
        self.options_relase = self.get_dists()
        drop_types = OptionMenu(root, self.clicked_release, *self.options_relase)
        drop_types.grid(row=1, column=2)

        # Call the update method here to populate the second set of dropdowns initially
        self.update_sdk_and_files()

        button = Button(root, text="Compare", command=self.packages_press_button)
        button.grid(row=4, column=2, pady=20)

    def help(self):
        help_popup = Toplevel()
        help_popup.title("Help")
        
        usage_label = Label(help_popup, text="==========" * 7 +"\n"
                                        "Welcome to the Modal AI CHANGELOG generator.\n"
                                        "Follow these steps to use the application:\n"
                                        "1. Select a release from the dropdown menu.\n"
                                        "2. Choose SDK versions to compare.\n"
                                        "3. Select package files to compare.\n"
                                        "4. Click the 'Compare' button to generate the changelog.\n"
                                        "5. Use 'Export as MD' or 'Export as TXT' to save the changelog.\n"
                                        "Please note that you need to provide your GitLab token for more detailed changes on certain services.\n"
                                        "=======================================================================")
        
        usage_label.pack(padx=10, pady=10)
        


    def update_sdk_and_files(self, *args):
        # Update the SDK dropdowns
        
        options_sdk = self.get_list_sdk(self.clicked_release.get())
        for widget in root.winfo_children():
            widget.grid_forget()
        drop_types = OptionMenu(root, self.clicked_release, *self.options_relase)
        drop_types.grid(row=1, column=2)
        if options_sdk:
            drop_compare_sdk1 = OptionMenu(root, self.clicked_sdk1, *options_sdk)
            drop_compare_sdk1.grid(row=2, column=0, padx=15)
            drop_compare_sdk2 = OptionMenu(root, self.clicked_sdk2, *options_sdk)
            drop_compare_sdk2.grid(row=2, column=3, padx=15)

            # Update the file dropdowns
            options_file1 = self.get_file(self.clicked_release.get(), self.clicked_sdk1.get())
            options_file2 = self.get_file(self.clicked_release.get(), self.clicked_sdk2.get())
            if options_file1 and options_file2:
                drop_compare_files1 = OptionMenu(root, self.clicked_file1, *options_file1)
                drop_compare_files1.grid(row=3, column=0, padx=15)
                drop_compare_files2 = OptionMenu(root, self.clicked_file2, *options_file2)
                drop_compare_files2.grid(row=3, column=3, padx=15)
            
            button = Button(root, text="Compare", command=self.packages_press_button)
            button.grid(row=4, column=2, pady=20)
    

        

    def packages_press_button(self):
        
        sdk1_tocompare = self.clicked_sdk1.get()
        sdk2_tocompare = self.clicked_sdk2.get()
        sdk_type = self.clicked_release.get()
        package_name1 = self.clicked_file1.get()
        package_name2 = self.clicked_file2.get()
        print(sdk1_tocompare + " and " + sdk2_tocompare)
        url_1 = BASE_URL + sdk_type + sdk1_tocompare + "binary-arm64/"
        link_1 = url_1 + self.get_links(url_1, package_name1)
        file_1 = self.download_and_delete_file(link_1)
        self.file_1_dict = self.create_package_dict(file_1)
        print(file_1)
        url_2 = BASE_URL + sdk_type + sdk2_tocompare + "binary-arm64/"
        link_2 = url_2 + self.get_links(url_2, package_name2)
        file_2 = self.download_and_delete_file(link_2)
        self.file_2_dict = self.create_package_dict(file_2)
        print(file_2)
        difftable_button = Button(root, text = "export version diff table", command = self.generate_diff_table)
        difftable_button.grid(row=5, column=1)
        changelog_button = Button(root, text = "export detailed changelog", command = self.generate_changelog)
        changelog_button.grid(row=5, column=3)
        release_notes_button = Button(root, text = "export complete table\nfor release notes", command = self.generate_release_notes)
        release_notes_button.grid(row = 5, column=2)
        row_instant = 7
        for widget in root.winfo_children():
            if isinstance(widget, Label):
                widget.grid_remove()
        
        # token_label = Label(root, text="Enter GitLab Token:")
        # token_label.grid(row=6, column=0, padx=10, pady=5)
        # self.gitlab_token_entry = Entry(root)
        # self.gitlab_token_entry.grid(row=6, column=1, padx=10, pady=5)
        
        for package in self.file_1_dict:
            if package in self.file_2_dict:
                if self.file_2_dict[package] != self.file_1_dict[package]:
                    diff_label_sdk1 = Label(root, text = self.file_1_dict[package], font=('Arial', 9))
                    diff_label_sdk1.grid(row = row_instant, column = 0)
                    diff_label_sdk2 = Label(root, text=self.file_2_dict[package], font=('Arial', 9))
                    package_label = Label(root, text= package, font=('Arial', 9))
                    package_label.grid(row=row_instant, column=1)
                    diff_label_sdk2.grid(row = row_instant, column=3)
                    row_instant = row_instant +1
            else:
                diff_label_sdk1 = Label(root, text = self.file_1_dict[package], font=('Arial', 9))
                diff_label_sdk1.grid(row = row_instant, column = 0)
                diff_label_sdk2 = Label(root, text= "none", font=('Arial', 9))
                diff_label_sdk2.grid(row = row_instant, column=3)
                package_label = Label(root, text= package, font=('Arial', 9))
                package_label.grid(row=row_instant, column=1)
                row_instant = row_instant +1
        for package in self.file_2_dict:
            if package not in self.file_1_dict:
                diff_label_sdk1 = Label(root, text= "none", font=('Arial', 9))
                diff_label_sdk1.grid(row=row_instant, column=0)
                diff_label_sdk2 = Label(root, text=  self.file_2_dict[package], font=('Arial', 9))
                diff_label_sdk2.grid(row=row_instant, column=3)
                package_label = Label(root, text= package, font=('Arial', 9))
                package_label.grid(row=row_instant, column=1)
                row_instant += 1
    

    def generate_release_notes(self):
        loading = Label(root, text = "Loading ....", font = ("Trebuchet Ms", 13, "bold"), fg = "#FFFFFF", bg = "#2F6C60")
        loading.grid(row=5, column=1)

        self.filepath=filedialog.askdirectory(initialdir="~/Desktop/",
                                title="Choose a Directory")
        self.save_path=self.filepath

        with open(self.save_path + "/version_table_for_release_notes.md", "w") as file:
            file.write("| "+" Package "+" | " + self.clicked_sdk1.get() + " | " + self.clicked_sdk2.get() + " | \n")
            file.write("|:---|:---:|:---:| \n")
            for package in self.file_1_dict:
                if package in self.file_2_dict:
                    if self.file_2_dict[package] != self.file_1_dict[package]:
                        file.write("| <mark>" + package + "</mark> | " + self.file_1_dict[package] + " | " + self.file_2_dict[package] + " | \n")
                    else:
                        file.write("| " + package + " | " + self.file_1_dict[package] + " | " + self.file_2_dict[package] + " | \n")
                else:
                    file.write("| <mark>" + package + "</mark> | " + self.file_1_dict[package] + " | " + "none" + " | \n" )
            for package in self.file_2_dict:
                if package not in self.file_1_dict:
                    file.write("| <mark>" + package + "</mark> | " + "none" + " | " + self.file_2_dict[package] + " | \n" )
        loading.grid_remove()

    def generate_diff_table(self):
        loading = Label(root, text = "Loading ....", font = ("Trebuchet Ms", 13, "bold"), fg = "#FFFFFF", bg = "#2F6C60")
        loading.grid(row=5, column=1)

        self.filepath=filedialog.askdirectory(initialdir="~/Desktop/",
                                title="Choose a Directory")
        self.save_path=self.filepath

        with open(self.save_path + "/simple_version_diff_table.md", "w") as file:
            file.write("| "+" Package "+" | " + self.clicked_sdk1.get() + " | " + self.clicked_sdk2.get() + " | \n")
            file.write("|:---|:---:|:---:| \n")
            for package in self.file_1_dict:
                if package in self.file_2_dict:
                    if self.file_2_dict[package] != self.file_1_dict[package]:
                        file.write("| " + package + " | " + self.file_1_dict[package]+ " | " + self.file_2_dict[package] + " | \n")
                else:
                    file.write("| " + package + " | " + self.file_1_dict[package] + " | " + "none" + " | \n")
            for package in self.file_2_dict:
                if package not in self.file_1_dict:
                    file.write("| " + package + " | " + "none" + " | " + self.file_2_dict[package] + " | \n")
        loading.grid_remove()



    def generate_changelog(self):
        loading = Label(root, text = "Loading All CHANGLOG files from\nGitLab, please be patient....", font = ("Trebuchet Ms", 13, "bold"), fg = "#FFFFFF", bg = "#2F6C60")
        loading.grid(row=5, column=1)
        self.filepath=filedialog.askdirectory(initialdir="~/Desktop/", title="Choose a Directory")
        self.save_path=self.filepath + "/full_changelog.txt"
        id_dict = {

            }
        with open('gitlab-projectids.txt') as f:
            lines = f.readlines()
            for line in lines:
                sett = line.split(":")
                package = sett[0]
                print(package)
                # if "\n" in sett[1]:
                #     project_id = sett[1].removesuffix("\n")
                # else:
                #     project_id = sett[1]
                id_dict[package] = sett[1].strip()
        

        with open(self.save_path, "w") as file:

            if os.path.isfile("./gitlab_token.txt"):
                with open("./gitlab_token.txt", "r") as gitfile:
                    token = gitfile.readline().strip()
                    # print("using token: " + token)
                    gl = gitlab.Gitlab(url="https://gitlab.com", private_token=token)
            else:
                print("opening gitlab with no gitlab token!")
                gl = gitlab.Gitlab(url="https://gitlab.com")

            for package in self.file_1_dict:
                if package in self.file_2_dict:
                    if self.file_2_dict[package] != self.file_1_dict[package]:
                        print("fetching CHANGELOG for: " + package)
                        # file.write(package + "\n")
                        if package in id_dict:
                            try:
                                project = gl.projects.get(id_dict[package])
                                file_info = project.files.get(file_path="CHANGELOG", ref="master")
                                content_base64 = file_info.content.encode()
                                content_bytes = base64.b64decode(content_base64)
                                content_str = content_bytes.decode("utf-8")
                                start_version="unknown"
                                end_version="unknown"
                                # if "-" not in self.file_2_dict[package] and "-" not in self.file_1_dict[package] and self.file_1_dict[package] in content_str and self.file_2_dict[package] in content_str:
                                version_components1 = list(map(int, self.file_2_dict[package].replace("-",".").split(".")))
                                version_components2 = list(map(int, self.file_1_dict[package].replace("-",".").split(".")))
                                is_version1_greater = version_components1 > version_components2
                                if is_version1_greater:
                                    start_version = self.file_2_dict[package]
                                    end_version = self.file_1_dict[package]
                                else:
                                    start_version = self.file_1_dict[package]
                                    end_version = self.file_2_dict[package]
                                # Extract changes between the versions
                                start_marker = start_version + "\n"
                                end_marker = "\n" + end_version + "\n"
                                start_index = content_str.find(start_marker)
                                end_index = content_str.find(end_marker, start_index)
                                if start_index == -1:
                                    start_index = 0
                                if start_index != -1 and end_index != -1:
                                    file.write(package + "  (" + end_version + " to " + start_version + ")\n")
                                    file.write(content_str[start_index:end_index]+"\n")
                                    file.write("\n\n")
                                    # else:
                                    #     file.write(package + "failed to parse changelog\n")
                                else:
                                    print("\nWARNING: Bad Changelog in: " + package + ", writing out entire changelog instead\n")
                                    print(start_version , start_marker , start_index , end_version , end_marker , end_index)
                                    file.write(package + "  (" + end_version + " to " + start_version + ")\n")
                                    file.write(content_str+"\n")
                                    file.write("\n\n")

                            except gitlab.exceptions.GitlabGetError as e:
                                print("Error:", e)
                        else:
                            file.write(package + " no longer present or has been renamed")
                            print(package + " no longer present or has been renamed")
                
                else:
                    file.write(package + "\n")
                    if package in id_dict:
                        
                        if os.path.isfile("./gitlab_token.txt"):
                            with open("./gitlab_token.txt", "r") as gitfile:
                                token = gitfile.readline().strip()
                                gl = gitlab.Gitlab(url="https://gitlab.com", private_token=token)
                        # elif self.gitlab_token_entry.get():
                        #     gl = gitlab.Gitlab(url="https://gitlab.com", private_token=self.gitlab_token_entry.get())
                        #     with open("gitlab_token.txt", "w") as writegit:
                        #         writegit.write(self.gitlab_token_entry.get())
                        else:
                            gl = gitlab.Gitlab(url="https://gitlab.com")
                        try:
                            # Fetch the project
                            project = gl.projects.get(id_dict[package])
                            # Fetch the content of the file using the API URL
                            file_info = project.files.get(file_path="CHANGELOG", ref="master")
                            content_base64 = file_info.content.encode()
                            content_bytes = base64.b64decode(content_base64)
                            content_str = content_bytes.decode("utf-8")
                            file.write(content_str+"\n")
                        except gitlab.exceptions.GitlabGetError as e:
                            print("Error:", e)
                    else:
                        print(package +" not in database")
            
            for package in self.file_2_dict:
                
                if package not in self.file_1_dict:
                    file.write(package + "\n")
                    if package in id_dict:
                        
                        if os.path.isfile("./gitlab_token.txt"):
                            with open("./gitlab_token.txt", "r") as gitfile:
                                token = gitfile.readline().strip()
                                gl = gitlab.Gitlab(url="https://gitlab.com", private_token=token)
                        # elif self.gitlab_token_entry.get():
                        #     gl = gitlab.Gitlab(url="https://gitlab.com", private_token=self.gitlab_token_entry.get())
                        #     with open("gitlab_token.txt", "w") as writegit:
                        #         writegit.write(self.gitlab_token_entry.get())
                        else:
                            gl = gitlab.Gitlab(url="https://gitlab.com")
                        try:
                            # Fetch the project
                            project = gl.projects.get(id_dict[package])
                            # Fetch the content of the file using the API URL
                            file_info = project.files.get(file_path="CHANGELOG", ref="master")
                            content_base64 = file_info.content.encode()
                            content_bytes = base64.b64decode(content_base64)
                            content_str = content_bytes.decode("utf-8")
                            file.write(content_str +"\n")
                        except gitlab.exceptions.GitlabGetError as e:
                            print("Error:", e)
                    else:
                        print(package + " not in database")
            file.close()
            loading.grid_remove()
            open_with_editor(self.save_path)

    
    def get_list_sdk(self, type):
        url = "http://voxl-packages.modalai.com/dists/" + type
        r = requests.get(url)
        soup = Soup(r.text, "html.parser")
        releases = soup.find_all("a")
        releases_string = []
        for sdk in releases:
            sdk_string = sdk.text
            # if "/" in sdk_string:
            releases_string.append(sdk.text)
        return releases_string
    
    def get_dists(self):
        url = "http://voxl-packages.modalai.com/dists/"
        r = requests.get(url)
        soup = Soup(r.text, "html.parser")
        releases = soup.find_all("a")
        releases_string = []
        for release in releases:
            sdk_string = release.text
            if "/" in sdk_string:
                releases_string.append(release.text)
        return releases_string

    def get_links(self, url, package):
        req = requests.get(url)
        url_data = Soup(req.text, "html.parser")
        links = url_data.find_all("a")
        for link in links:
            if package in link["href"]:
                link_to_use = link.get("href")
                break
        return link_to_use
    
    def delete_temp_directory(self, temp_dir):
        if os.path.exists(temp_dir):
            for root, dirs, files in os.walk(temp_dir, topdown=False):
                for name in files:
                    os.remove(os.path.join(root, name))
                for name in dirs:
                    os.rmdir(os.path.join(root, name))
            os.rmdir(temp_dir)

    def download_and_delete_file(self, url):
        if "Packages" in url:
            if "Packages.gz" in url:
                tmp_dir = "/tmp/gz_extraction"
                os.makedirs(tmp_dir, exist_ok=True)

                # Register the delete_temp_directory function to be called on program exit
                atexit.register(self.delete_temp_directory, tmp_dir)

                # Download the .gz file
                response = requests.get(url)
                if response.status_code != 200:
                    raise ValueError("Failed to download the .gz file.")

                # Save the .gz file to the temporary directory
                gz_file_path = os.path.join(tmp_dir, "packages.gz")
                with open(gz_file_path, "wb") as f:
                    f.write(response.content)
                
                # Extract the Packages file from the .gz archive
                with gzip.open(gz_file_path, 'rb') as f_in:
                    extracted_data = f_in.read()
                    packages_file_path = os.path.join(tmp_dir, "Packages")
                    with open(packages_file_path, 'wb') as f_out:
                        f_out.write(extracted_data)

                return packages_file_path
            
            else:
                response = requests.get(url)
                print(url)
                if response.status_code == 200:
                    temp_file = tempfile.NamedTemporaryFile(delete=False)
                    temp_file.write(response.content)
                    temp_file.close()
                    print(f"File downloaded successfully and saved at: {temp_file.name}")

                    # Register a function to be called when the script exits
                    atexit.register(self.delete_temp_file, temp_file.name)
                    return temp_file.name
        elif "suite" in url:
            # Create a temporary directory in /tmp to store the downloaded and extracted files
            tmp_dir = "/tmp/deb_extraction"
            os.makedirs(tmp_dir, exist_ok=True)

            # Register the delete_temp_directory function to be called on program exit
            atexit.register(self.delete_temp_directory, tmp_dir)

            # Download the .deb file
            response = requests.get(url)
            if response.status_code != 200:
                raise ValueError("Failed to download the .deb file.")

            deb_file_path = os.path.join(tmp_dir, "package.deb")

            # Save the .deb file to the temporary directory
            with open(deb_file_path, "wb") as f:
                f.write(response.content)
                f.close()
                
            subprocess.run(["ar", "-x", "/tmp/deb_extraction/package.deb"])
            subprocess.run(["tar", "-xf", "control.tar.xz"])
            return "control"


    def delete_temp_file(self, file_path):
        try:
            os.remove(file_path)
            print(f"File deleted successfully: {file_path}")
        except Exception as e:
            print(f"Failed to delete the file: {e}")

    def get_file(self, release, sdk):
        url = "http://voxl-packages.modalai.com/dists/" + release + sdk +"binary-arm64/"

        print(url)
        r = requests.get(url)
        print(r)


        soup = Soup(r.text, "html.parser")
        files = soup.find_all("a")
        files_list = []
        for file in files:
            file_string = file.text
            if "Packages" in file_string :
                files_list.append(file.text)
            elif "suite" in file_string:
                files_list.append(file.text)
                
        print(files_list)
        return files_list

    def create_package_dict(self, path):
        # print("create_package_dict for: " + path)

        if "/tmp" in path:
            f = open(path, "r")
            file_data = f.readlines()
            data_dict = {

            }
            package_name = ""
            for line in file_data:
                if "Package" in line:
                    package = line.split(":")
                    package_name = package[1].strip()
                    # print("package_name: " + package_name)
                elif "Version" in line:
                    version_data = line.split(":")
                    version = version_data[1].strip()
                    data_dict[package_name] = version
                    package_name = ""
                    # print("version: " + version)
        elif "control" in path:
            f = open("control", "r")
            file_data = f.readlines()
            data_dict = {

            }
            parsed_data = file_data[5].split()
            parsed_data.remove("Depends:")
            for word in parsed_data:
                list_of_services = word.split("(")
                service = list_of_services[0]
                version = list_of_services[1]
                version = re.sub(r'^.*?(\d)', r'\1', version).split(")")

                data_dict[service] = version[0]

        
        print(data_dict)
        return data_dict



root = Tk()
MyApp(root)
root.mainloop()
